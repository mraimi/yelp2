#! /usr/bin/python
from sklearn.linear_model import SGDClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from stat_gen import Data_gen
from sklearn.feature_extraction.text import TfidfTransformer
import json, sys, pickle
from sklearn import metrics
from sklearn.pipeline import Pipeline
import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
class Bow:
	def __init__(self):
		self.vectorizer = CountVectorizer(	analyzer = "word", 	\
							tokenizer = None,	\
							preprocessor = None,	\
							stop_words = None,	\
							max_features = 5000)
		self.review_list = []
		self.labels = []
		self.test_labels = []
		self.test_data = []
		
		self.train_data_features = None
		self.e_test_reviews = []
		self.ne_test_reviews = []
		self.forest = None
		self.e_results = None
		self.ne_results = None
		self.svm = None
		self.svm_test_reviews = None

	def read_to_list(self):
		elite_reviews = open('yelp_dataset_challenge_academic_dataset/clean_elite_reviews.txt','r')
		non_elite_reviews = open('yelp_dataset_challenge_academic_dataset/clean_non_elite_reviews.txt','r')
		#lst_pkl = open('training.pkl','r')
		#CHANGE TO WRITE IF REDOING THIS PROCESS

		limit = 100000
		count = 1
		elen = 0.0
		nelen = 0.0
		# reading all reviews into one list
		for line in elite_reviews:
			if (count <= limit):
				count += 1
				#self.review_list.append(line.strip())
				elen += len(line.strip().split())
			else:
				count = 1
				break
		for line in non_elite_reviews:
			if (count <= limit):
				count += 1
				nelen += len(line.strip().split())
				#self.review_list.append(line.strip())
			else:
				break
		#pickle.dump(self.review_list, lst_pkl)
		#lst_pkl.close()
		elite_reviews.close()
		non_elite_reviews.close()
		print("avg elite: " + str(elen/100000))
		print("avg not-elite " + str(nelen/100000))
	def test_to_list(self):
		clean_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_elite_test_reviews.txt','r')
		clean_n_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_n_elite_test_reviews.txt','r')
		dg = Data_gen()
		for line in clean_elite_test_reviews:
		 	self.test_data.append(dg.clean(line))
		clean_elite_test_reviews.close()
		for line in clean_n_elite_test_reviews:
			self.test_data.append(dg.clean(line))
		clean_n_elite_test_reviews.close()
		
		td = open('yelp_dataset_challenge_academic_dataset/test_data_list.pkl','r')
		#CHANGE TO WRITE IF REDOING THIS PROCESS
		pickle.dump(self.test_data,td)
		td.close()



	def load_data(self):
		#this is the pickled training data !!LIST!!
		lst_pkl = open('yelp_dataset_challenge_academic_dataset/training.pkl','r')
		self.review_list = pickle.load(lst_pkl)
		lst_pkl.close()
		#for line in self.review_list:
		#	print line
		#print "length of review_list: " + str(len(self.review_list))

		td = open('yelp_dataset_challenge_academic_dataset/test_data_list.pkl','r')
		self.review_list = pickle.load(td)
		td.close()

	def create_bow(self):
		self.forest = RandomForestClassifier(n_estimators = 100)
	
		# vocab = self.vectorizer.get_feature_names()

	def create_labels(self):
		self.labels = np.ones(100000)
		self.labels = np.append(self.labels, np.zeros(100000)).tolist()
		self.test_labels = np.ones(50000)
		self.test_labels = np.append(self.test_labels,np.zeros(50000)).tolist()
		# self.labels = np.ones(100000).tolist() + np.zeros(100000).tolist()
		# self.test_labels = np.ones(50000).tolist() + np.zeros(50000).tolist()

	def random_forest(self):
		#forest_pkl = open('forest.pkl','w')
		self.forest = RandomForestClassifier(n_estimators = 100) 
		self.forest = self.forest.fit(self.train_data_features, self.labels)
		#pickle.dump(forest,forest_pkl)
		#forest_pkl.close()

	def rf_baseline(self):
		clean_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_elite_test_reviews.txt','rb')
		clean_n_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_n_elite_test_reviews.txt','rb')
		#forest_pkl = open('yelp_dataset_challenge_academic_dataset/forest.pkl','r')
		#forest = pickle.load(forest_pkl)
		#forest_pkl.close()
		
		dg = Data_gen()
		count = 1
		for line in clean_elite_test_reviews:
		 	self.e_test_reviews.append(dg.clean(line))
		clean_elite_test_reviews.close()
		for line in clean_n_elite_test_reviews:
			self.ne_test_reviews.append(dg.clean(line))
		clean_n_elite_test_reviews.close()
		test_e_data_features = self.vectorizer.transform(self.e_test_reviews)
		test_e_data_features = test_e_data_features.toarray()
		self.e_results = self.forest.predict(test_e_data_features)
		
		test_ne_data_features = self.vectorizer.transform(self.ne_test_reviews)
		test_ne_data_features = test_ne_data_features.toarray()
		self.ne_results = self.forest.predict(test_ne_data_features)

		#results_pkl = open('ne_preds.pkl','w')
		#pickle.dump(result,results_pkl)
		#results_pkl.close()

		

	def score_bow(self):
		#ne_results_pkl = open('yelp_dataset_challenge_academic_dataset/ne_preds.pkl','r')
		#e_results_pkl = open('yelp_dataset_challenge_academic_dataset/e_preds.pkl','r')

		#e_preds = pickle.load(e_results_pkl)
		#ne_preds = pickle.load(ne_results_pkl)
		e_sum = np.sum(self.e_results)
		ne_sum = 50000 - np.sum(self.ne_results)

		print "BoW performance: " + str((e_sum + ne_sum)/100000)

		#ne_results_pkl.close()
		#e_results_pkl.close()

	def svm_baseline(self):
		clean_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_elite_test_reviews.txt','r')
		clean_n_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_n_elite_test_reviews.txt','r')
		test_features_pkl = open('yelp_dataset_challenge_academic_dataset/test_features.pkl','w');
		self.svm = LinearSVC()
		self.svm.fit(self.train_data_features, self.labels)
		self.svm.predict(self.train_data_features)
		self.svm.score(self.train_data_features, self.labels)

		dg = Data_gen()
		for line in clean_elite_test_reviews:
		 	self.svm_test_reviews.append(dg.clean(line))
		clean_elite_test_reviews.close()
		for line in clean_n_elite_test_reviews:
			self.svm_test_reviews.append(dg.clean(line))
		clean_n_elite_test_reviews.close()
		svm_test_features = self.vectorizer.transform(self.svm_test_reviews)
		svm_test_features = test_e_data_features.toarray()
		pickle.dump(svm_test_features, test_features_pkl)
		test_features_pkl.close()

		svm.score(svm_test_features, self.test_labels)



	def getLengths(self, mode):
		if (mode == "TRAIN"):
			filedesc = open('yelp_dataset_challenge_academic_dataset/clean_combined_training.txt','r')
		elif (mode == "TEST"):
			filedesc = open('yelp_dataset_challenge_academic_dataset/clean_combined_test.txt','r')	
		
		lengths = []
		# total = 0.0
		count = 0
		for line in filedesc:
			a = len(line.strip().split())
			lengths.append(a)
			count += 1
		print count
		filedesc.close()
		# lst_pkl.close()
		return np.asarray(lengths).reshape(-1,1)

	def pipeBow(self):
		# lst_pkl = open('yelp_dataset_challenge_academic_dataset/training.pkl','r')
		comb = open('yelp_dataset_challenge_academic_dataset/clean_combined_training.txt','r')
		t_comb = open('yelp_dataset_challenge_academic_dataset/clean_combined_test.txt','r')	
	#	text_clf = Pipeline([('cv', CountVectorizer()),('tdidf', TfidfTransformer()),('clf',SVC())])
		# text_clf = Pipeline([('cv', self.vectorizer),('clf',MultinomialNB())])
		# text_clf.fit(comb,self.labels)
		# predicted = text_clf.predict(t_comb)
		# print(metrics.classification_report(self.test_labels,predicted))
		
		#----------Pre-pipeline method--------------#
		counts = self.vectorizer.fit_transform(comb)
		print counts.shape
		lengths = self.getLengths("TRAIN")
		print lengths.shape
		counts = np.hstack((counts.todense(),lengths))
		print counts.shape
		scaler = StandardScaler().fit(counts)
		scaler.transform(counts)
		# tf_trans = TfidfTransformer()
		# x_train_tfidf = tf_trans.fit_transform(counts)
		# text_clf = text_clf.fit(np.asarray(self.review_list), np.asarray(self.labels))
		clf = MultinomialNB().fit(counts,self.labels) 
		test_counts = self.vectorizer.fit_transform(t_comb)
		t_lengths = self.getLengths("TEST")
		test_counts = np.hstack((test_counts.todense(),t_lengths))	
		scaler.transform(test_counts)
		# x_test_tfidf = tf_trans.fit_transform(test_counts)
		predicted = clf.predict(test_counts)
		# print predicted.shape
		print(metrics.classification_report(self.test_labels,predicted))

	def unifyData(self):
		elite_reviews = open('yelp_dataset_challenge_academic_dataset/clean_elite_reviews.txt','r')
		non_elite_reviews = open('yelp_dataset_challenge_academic_dataset/clean_non_elite_reviews.txt','r')
		comb = qopen('yelp_dataset_challenge_academic_dataset/clean_combined_training.txt','w')
		limit = 100000
		count = 1
		for line in elite_reviews:
			if (count <= limit):
				count += 1
				comb.write(line)
			else:
				count = 1
				break
		for line in non_elite_reviews:
			if (count <= limit):
				count += 1
				comb.write(line)
			else:
				break
		elite_reviews.close()
		non_elite_reviews.close()
	
		t_comb = qopen('yelp_dataset_challenge_academic_dataset/clean_combined_test.txt','r')	
		clean_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_elite_test_reviews.txt','r')
		clean_n_elite_test_reviews = open('yelp_dataset_challenge_academic_dataset/clean_n_elite_test_reviews.txt','r')
		for line in clean_elite_test_reviews:
		 	t_comb.write(line)
		clean_elite_test_reviews.close()
		for line in clean_n_elite_test_reviews:
			t_comb.write(line)
		clean_n_elite_test_reviews.close()

		

if __name__ == "__main__":
	bow = Bow()
	#-------------------
	#bow.read_to_list()
	bow.load_data()
	# bow.test_to_list()
	#-------------------
	# bow.create_bow()
	bow.create_labels()
	#------------------
	# bow.random_forest()
	# bow.svm_baseline()
	#-------------------
	# bow.rf_baseline()
	#-------------------
	# bow.score_bow()
	bow.pipeBow()
	# bow.getLengths()
	# bow.unifyData()



